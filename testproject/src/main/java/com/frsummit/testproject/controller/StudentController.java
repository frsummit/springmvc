/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frsummit.testproject.controller;

import com.frsummit.testproject.model.Student;
import com.frsummit.testproject.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author F R SUMMIT
 */
@Controller
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addStudent(
            @RequestParam String studentId, 
            @RequestParam String studentName, 
            @RequestParam double cgpa){
        Student student = new Student(studentId, studentName, cgpa);
        System.out.println(student);
        studentRepository.save(student);
        return "save";
    }
}
