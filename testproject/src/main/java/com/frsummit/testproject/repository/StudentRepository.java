/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frsummit.testproject.repository;

import com.frsummit.testproject.model.Student;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author F R SUMMIT
 */
public interface StudentRepository extends CrudRepository<Student, Long> {
    
}
